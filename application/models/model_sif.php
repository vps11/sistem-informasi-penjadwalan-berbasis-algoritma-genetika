<?php

class Model_Sif extends CI_Model {

    public $limit;
    public $offset;
    public $sort;
    public $order;

    function __construct() {
        parent::__construct();
    }

    function get() {
        return $this->db->query(
                        "SELECT A.kode, " .
                        "A.nama, " .
                        "A.karyawan_per_sif, " .
                        "A.kode_jabatan, " .
                        "B.nama AS jabatan, " .
                        "A.kode_pengubah " .
                        "FROM sif A " .
                        "LEFT JOIN jabatan B ON A.kode_jabatan = B.kode " .
                        "WHERE A.terhapus = 'N'"
        );
    }

    function get_by_kode($kode) {
        return $this->db->query(
                        "SELECT A.kode, " .
                        "A.nama, " .
                        "A.karyawan_per_sif, " .
                        "A.kode_jabatan, " .
                        "B.nama AS jabatan, " .
                        "A.kode_pengubah " .
                        "FROM sif A " .
                        "LEFT JOIN jabatan B ON A.kode_jabatan = B.kode " .
                        "WHERE A.kode = $kode " .
                        "AND A.terhapus = 'N'"
        );
    }

    function get_by_jabatan($kode) {
        return $this->db->query(
                        "SELECT A.kode, " .
                        "A.nama, " .
                        "A.karyawan_per_sif, " .
                        "A.kode_jabatan, " .
                        "B.nama AS jabatan, " .
                        "A.kode_pengubah " .
                        "FROM sif A " .
                        "LEFT JOIN jabatan B ON A.kode_jabatan = B.kode " .
                        "WHERE A.kode_jabatan = $kode " .
                        "AND A.terhapus = 'N'"
        );
    }

    function insert($data) {
        $sif = [];
        $rs_sif = $this->db->query("SELECT kode FROM sif WHERE terhapus = 'Y'");
        $i = 0;
        foreach ($rs_sif->result() as $data) {
            $sif[$i] = intval($data->kode);
            $i++;
        }
        if (count($sif) === 0) {
            $this->db->insert('sif', $data);
        }
        else {
            $this->db->where('kode', $sif[0]);
            $this->db->update('sif', $data);
            $this->db->query("UPDATE sif SET terhapus = 'N' WHERE kode = $sif[0]");
        }
    }

    function update($kode, $data) {
        $this->db->where('kode', $kode);
        $this->db->update('sif', $data);
    }

    function delete($kode) {
        $this->db->query("UPDATE sif SET terhapus = 'Y' WHERE kode = $kode");
    }

}
