<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_Pengguna extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function create_user($username, $email, $password) {
        $data = [
            'username' => $username,
            'email' => $email,
            'password' => $this->hash_password($password)
        ];
        return $this->db->insert('pengguna', $data);
    }

    public function resolve_user_login($username, $password) {
        $this->db->select('password');
        $this->db->from('pengguna');
        $this->db->where('username', $username);
        $hash = $this->db->get()->row('password');
        return $this->verify_password_hash($password, $hash);
    }

    public function get_user_id_from_username($username) {
        $this->db->select('id');
        $this->db->from('pengguna');
        $this->db->where('username', $username);
        return $this->db->get()->row('id');
    }

    public function get_user($user_id) {
        $this->db->from('pengguna');
        $this->db->where('id', $user_id);
        return $this->db->get()->row();
    }

    private function hash_password($password) {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    private function verify_password_hash($password, $hash) {
        return password_verify($password, $hash);
    }

}
