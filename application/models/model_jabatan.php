<?php

class Model_Jabatan extends CI_Model {

    public $limit;
    public $offset;
    public $sort;
    public $order;

    function __construct() {
        parent::__construct();
    }

    function get() {
        return $this->db->query(
                        "SELECT kode, " .
                        "nama " .
                        "FROM jabatan " .
                        "WHERE terhapus = 'N'"
        );
    }

    function get_by_kode($kode) {
        return $this->db->query(
                        "SELECT kode , " .
                        "nama " .
                        "FROM jabatan " .
                        "WHERE kode = $kode " .
                        "AND terhapus = 'N'"
        );
    }

    function insert($data) {
        $jabatan = [];
        $rs_jabatan = $this->db->query("SELECT kode FROM jabatan WHERE terhapus = 'Y'");
        $i = 0;
        foreach ($rs_jabatan->result() as $data) {
            $jabatan[$i] = intval($data->kode);
            $i++;
        }
        if (count($jabatan) === 0) {
            $this->db->insert('jabatan', $data);
        }
        else {
            $this->db->where('kode', $jabatan[0]);
            $this->db->update('jabatan', $data);
            $this->db->query("UPDATE jabatan SET terhapus = 'N' WHERE kode = $jabatan[0]");
        }
    }

    function update($kode, $data) {
        $this->db->where('kode', $kode);
        $this->db->update('jabatan', $data);
    }

    function delete($kode) {
        $this->db->query("UPDATE jabatan SET terhapus = 'Y' WHERE kode = $kode");
    }

}
