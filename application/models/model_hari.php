<?php

class Model_Hari extends CI_Model {

    public $limit;
    public $offset;
    public $sort;
    public $order;

    function __construct() {
        parent::__construct();
    }

    function get() {
        return $this->db->query(
                        "SELECT kode, " .
                        "nama " .
                        "FROM hari " .
                        "WHERE terhapus = 'N'"
        );
    }

    function get_kode($kode) {
        return $this->db->query(
                        "SELECT kode, " .
                        "nama " .
                        "FROM hari " .
                        "WHERE kode = $kode " .
                        "AND terhapus = 'N'"
        );
    }

    function insert($data) {
        $hari = [];
        $rs_hari = $this->db->query("SELECT kode FROM hari WHERE terhapus = 'Y'");
        $i = 0;
        foreach ($rs_hari->result() as $data) {
            $hari[$i] = intval($data->kode);
            $i++;
        }
        if (count($hari) === 0) {
            $this->db->insert('hari', $data);
        }
        else {
            $this->db->where('kode', $hari[0]);
            $this->db->update('hari', $data);
            $this->db->query("UPDATE hari SET terhapus = 'N' WHERE kode = $hari[0]");
        }
    }

    function update($kode, $data) {
        $this->db->where('kode', $kode);
        $this->db->update('hari', $data);
    }

    function delete($kode) {
        $this->db->query("UPDATE hari SET terhapus = 'Y' WHERE kode = $kode");
    }

}
