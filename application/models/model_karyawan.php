<?php

class Model_Karyawan extends CI_Model {

    public $limit;
    public $offset;
    public $sort;
    public $order;

    function __construct() {
        parent::__construct();
    }

    function get() {
        return $this->db->query(
                        "SELECT A.kode, " .
                        "A.nip, " .
                        "A.nama, " .
                        "A.golongan, " .
                        "A.kode_jabatan, " .
                        "B.nama AS jabatan " .
                        "FROM karyawan A " .
                        "LEFT JOIN jabatan B ON A.kode_jabatan = B.kode " .
                        "WHERE A.terhapus = 'N' " .
                        "ORDER BY $this->sort $this->order " .
                        "LIMIT $this->offset, $this->limit"
        );
    }

    function get_all() {
        return $this->db->query(
                        "SELECT A.kode, " .
                        "A.nip, " .
                        "A.nama, " .
                        "A.golongan, " .
                        "A.kode_jabatan, " .
                        "B.nama AS jabatan " .
                        "FROM karyawan A " .
                        "LEFT JOIN jabatan B ON A.kode_jabatan = B.kode " .
                        "WHERE A.terhapus = 'N' " .
                        "ORDER BY nama"
        );
    }

    function get_by_kode($kode) {
        return $this->db->query(
                        "SELECT A.kode, " .
                        "A.nip, " .
                        "A.nama, " .
                        "A.golongan, " .
                        "A.kode_jabatan, " .
                        "B.nama AS jabatan " .
                        "FROM karyawan A " .
                        "LEFT JOIN jabatan B ON A.kode_jabatan = B.kode " .
                        "WHERE A.terhapus = 'N' " .
                        "AND A.kode = '$kode'"
        );
    }

    function get_by_nama($nama) {
        return $this->db->query(
                        "SELECT A.kode, " .
                        "A.nip, " .
                        "A.nama, " .
                        "A.golongan, " .
                        "A.kode_jabatan, " .
                        "B.nama AS jabatan " .
                        "FROM karyawan A " .
                        "LEFT JOIN jabatan B ON A.kode_jabatan = B.kode " .
                        "WHERE A.terhapus = 'N' " .
                        "AND A.nama LIKE '%$nama%'"
        );
    }

    function insert($data) {
        $karyawan = [];
        $rs_karyawan = $this->db->query("SELECT kode FROM karyawan WHERE terhapus = 'Y'");
        $i = 0;
        foreach ($rs_karyawan->result() as $result) {
            $karyawan[$i] = intval($result->kode);
            $i++;
        }
        if (count($karyawan) === 0) {
            $this->db->insert('karyawan', $data);
        }
        else {
            $this->db->where('kode', $karyawan[0]);
            $this->db->update('karyawan', $data);
            $this->db->query("UPDATE karyawan SET terhapus = 'N' WHERE kode = $karyawan[0]");
        }
    }

    function update($kode, $data) {
        $this->db->where('kode', $kode);
        $this->db->update('karyawan', $data);
    }

    // Tidak dihapus karena memicu error. Karyawan yang dihapus biasanya akan digantikan posisinya.
    function delete($kode) {
        $this->db->query("UPDATE karyawan SET terhapus = 'Y' WHERE kode = $kode");
    }

}
