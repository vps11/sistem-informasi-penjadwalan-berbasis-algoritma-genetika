<?php

class Model_Jadwal_Kerja extends CI_Model {

    public $limit;
    public $offset;
    public $sort;
    public $order;

    function __construct() {
        parent::__construct();
    }

    function get() {
        return $this->db->query(
                        "SELECT A.nama AS hari, " .
                        "B.nama AS sif, " .
                        "C.nama AS karyawan, " .
                        "D.nama AS jabatan " .
                        "FROM jadwal_kerja E " .
                        "LEFT JOIN hari A ON E.kode_hari = A.kode " .
                        "LEFT JOIN sif B ON E.kode_sif = B.kode " .
                        "LEFT JOIN karyawan C ON E.kode_karyawan = C.kode " .
                        "LEFT JOIN jabatan D ON C.kode_jabatan = D.kode " .
                        "ORDER BY E.kode_hari ASC, E.kode_sif ASC, karyawan ASC"
        );
    }

    function truncate() {
        $this->db->query("TRUNCATE TABLE jadwal_kerja");
    }

    function update_pembuat($id) {
        $this->db->query(
                "UPDATE pengguna SET pembuat_jadwal_saat_ini = 'N' WHERE pembuat_jadwal_saat_ini = 'Y'"
        );
        $this->db->query(
                "UPDATE pengguna SET pembuat_jadwal_saat_ini = 'Y' WHERE id = $id"
        );
    }

}
