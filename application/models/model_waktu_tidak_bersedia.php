<?php

class Model_Waktu_Tidak_Bersedia extends CI_Model {

    public $limit;
    public $offset;
    public $sort;
    public $order;

    function __construct() {
        parent::__construct();
    }

    function get_by_kode($kode_karyawan) {
        $rs_wtb = NULL;
        if ($kode_karyawan != NULL) {
            $rs_wtb = $this->db->query(
                            "SELECT kode_hari, " .
                            "kode_sif, " .
                            "kode_karyawan, " .
                            "kode_pengubah " .
                            "FROM waktu_tidak_bersedia " .
                            "WHERE kode_karyawan = $kode_karyawan"
            );
        }
        return $rs_wtb;
    }

    function delete($kode_karyawan) {
        $this->db->query(
                "DELETE FROM waktu_tidak_bersedia " .
                "WHERE kode_karyawan = $kode_karyawan"
        );
    }

}
