<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Web extends CI_Controller {

    private $populasi;
    private $crossover;
    private $mutasi;
    private $jabatan;
    private $max_hari_kerja;
    private $individu = [[[[]]]]; // Array untuk PHP versi > 5.4.
    private $hari = [];
    private $sif = [[]];
    private $karyawan = [];
    private $karyawan_ijin = [];
    private $jumlah_hari;
    private $jumlah_sif;
    private $jumlah_karyawan;
    private $jumlah_posisi;
    private $jumlah_karyawan_ijin;
    private $waktu_karyawan_ijin = [[]];
    private $induk = [];

    function __construct() {
        parent::__construct();
        // Jika belum login.
        if (!isset($_SESSION['logged_in']) || !$_SESSION['logged_in']) {
            redirect('user/login');
        }
        // Memuat model-model CI.
        $this->load->model(['model_karyawan', 'model_sif', 'model_jabatan', 'model_hari', 'model_waktu_tidak_bersedia', 'model_jadwal_kerja']);
        define('IS_TEST', false);
    }

    function ambil_data() {
        // Mengumpulkan daftar kode karyawan.
        $rs_karyawan = $this->db->query("SELECT kode FROM karyawan WHERE terhapus = 'N' AND kode_jabatan LIKE '$this->jabatan'");
        $i = 0;
        foreach ($rs_karyawan->result() as $data) {
            $this->karyawan[$i] = intval($data->kode);
            $i++;
        }
        $this->jumlah_karyawan = count($this->karyawan);
        // Mengumpulkan daftar kode sif.
        $rs_sif = $this->db->query("SELECT kode, karyawan_per_sif FROM sif WHERE terhapus = 'N' AND kode_jabatan LIKE '$this->jabatan'");
        $i = 0;
        $this->jumlah_posisi = 0;
        foreach ($rs_sif->result() as $data) {
            $this->sif[$i][0] = intval($data->kode);
            $this->sif[$i][1] = intval($data->karyawan_per_sif);
            $this->jumlah_posisi += $this->sif[$i][1];
            $i++;
        }
        $this->jumlah_sif = count($this->sif);
        // Mengumpulkan daftar kode hari.
        $rs_hari = $this->db->query("SELECT kode FROM hari WHERE terhapus = 'N'");
        $i = 0;
        foreach ($rs_hari->result() as $data) {
            $this->hari[$i] = intval($data->kode);
            $i++;
        }
        $this->jumlah_hari = count($this->hari);
        $this->jumlah_posisi *= $this->jumlah_hari;
        // Mengumpulkan daftar waktu ketidaksediaan karyawan.
        // CONCAT_WS untuk penggabungan teks dengan suatu simbol.
        $rs_waktu_karyawan = $this->db->query(
                "SELECT kode_karyawan, " .
                "CONCAT_WS(':', kode_hari, kode_sif) as kode_hari_sif " .
                "FROM waktu_tidak_bersedia"
        );
        $i = 0;
        foreach ($rs_waktu_karyawan->result() as $data) {
            $this->karyawan_ijin[$i] = intval($data->kode_karyawan);
            $this->waktu_karyawan_ijin[$i][0] = intval($data->kode_karyawan);
            $this->waktu_karyawan_ijin[$i][1] = $data->kode_hari_sif;
            $i++;
        }
        $this->jumlah_karyawan_ijin = count($this->karyawan_ijin);
    }

    function inisiasi() {
        for ($i = 0; $i < $this->populasi; $i++) {
            $posisi = 0;
            for ($j = 0; $j < $this->jumlah_hari; $j++) {
                for ($k = 0; $k < $this->jumlah_sif; $k++) {
                    for ($l = 0; $l < $this->sif[$k][1]; $l++) {
                        $this->individu[$i][$posisi][0] = $posisi;
                        $this->individu[$i][$posisi][1] = $this->hari[$j];
                        $this->individu[$i][$posisi][2] = $this->sif[$k][0];
                        // Penentuan karyawan secara acak.
                        $this->individu[$i][$posisi++][3] = $this->karyawan[mt_rand(0, $this->jumlah_karyawan - 1)];
                    }
                }
            }
        }
    }

    function cek_fitness($individu) {
        $penalty = 0;
        $karyawan_sama = 0;
        for ($i = 0; $i < $this->jumlah_posisi; $i++) {
            $hari_a = intval($this->individu[$individu][$i][1]);
            $sif_a = intval($this->individu[$individu][$i][2]);
            $karyawan_a = intval($this->individu[$individu][$i][3]);
            for ($j = 0; $j < $this->jumlah_posisi; $j++) {
                $hari_b = intval($this->individu[$individu][$j][1]);
                $sif_b = intval($this->individu[$individu][$j][2]);
                $karyawan_b = intval($this->individu[$individu][$j][3]);
                // Jika posisi a === b.
                if ($i === $j) {
                    continue;
                }
                // Bentrok karyawan.
                if ($karyawan_a === $karyawan_b) {
                    if ($hari_a === $hari_b) {
                        $penalty++;
                    }
                    if ($karyawan_sama === $this->max_hari_kerja - 1) {
                        $penalty++;
                    }
                    else {
                        $karyawan_sama++;
                    }
                }
            }
            // Bentrok waktu keinginan karyawan.
            for ($j = 0; $j < $this->jumlah_karyawan_ijin; $j++) {
                if ($karyawan_a === $this->karyawan_ijin[$j]) {
                    $hari_sif = explode(':', $this->waktu_karyawan_ijin[$j][1]);
                    if ($hari_a === $hari_sif[0] && $sif_a === $hari_sif[1]) {
                        $penalty++;
                    }
                }
            }
            $karyawan_sama = 0;
        }
        $fitness = floatval(1 / (1 + $penalty));
        return $fitness;
    }

    function hitung_fitness() {
        for ($individu = 0; $individu < $this->populasi; $individu++) {
            $fitness[$individu] = $this->cek_fitness($individu);
        }
        return $fitness;
    }

    function seleksi($fitness) {
        $jumlah = 0;
        $rank = [];
        for ($i = 0; $i < $this->populasi; $i++) {
            // Proses ranking berdasarkan nilai fitness.
            $rank[$i] = 1;
            for ($j = 0; $j < $this->populasi; $j++) {
                // Ketika nilai fitness jadwal sekarang lebih dari nilai fitness jadwal yang lain.
                if (floatval($fitness[$i]) > floatval($fitness[$j])) {
                    $rank[$i] += 1;
                }
            }
            $jumlah += $rank[$i];
        }
        for ($i = 0; $i < $this->populasi; $i++) {
            // Proses seleksi berdasarkan ranking yang telah dibuat.
            $target = mt_rand(0, $jumlah - 1);
            $cek = 0;
            for ($j = 0; $j < $this->populasi; $j++) {
                $cek += $rank[$j];
                if (intval($cek) >= intval($target)) {
                    $this->induk[$i] = $j;
                    break;
                }
            }
        }
    }

    function crossover() {
        $individu_baru = [[[]]];
        for ($i = 0; $i < $this->populasi; $i += 2) {
            $b = 0;
            $cr = mt_rand(0, mt_getrandmax() - 1) / mt_getrandmax();
            // Two point crossover.
            if (floatval($cr) < floatval($this->crossover)) {
                // Ketika nilai random kurang dari nilai probabilitas pertukaran maka jadwal mengalami pertukaran.
                $a = mt_rand(0, $this->jumlah_posisi - 2);
                while ($b <= $a) {
                    $b = mt_rand(0, $this->jumlah_posisi - 1);
                }
                // Penentuan jadwal baru dari awal sampai titik pertama.
                for ($j = 0; $j < $a; $j++) {
                    for ($k = 0; $k < 4; $k++) {
                        $individu_baru[$i][$j][$k] = $this->individu[$this->induk[$i]][$j][$k];
                        $individu_baru[$i + 1][$j][$k] = $this->individu[$this->induk[$i + 1]][$j][$k];
                    }
                }
                // Penentuan jadwal baru dari titik pertama sampai titik kedua.
                for ($j = $a; $j < $b; $j++) {
                    for ($k = 0; $k < 4; $k++) {
                        $individu_baru[$i][$j][$k] = $this->individu[$this->induk[$i + 1]][$j][$k];
                        $individu_baru[$i + 1][$j][$k] = $this->individu[$this->induk[$i]][$j][$k];
                    }
                }
                // Penentuan jadwal baru dari titik kedua sampai akhir.
                for ($j = $b; $j < $this->jumlah_posisi; $j++) {
                    for ($k = 0; $k < 4; $k++) {
                        $individu_baru[$i][$j][$k] = $this->individu[$this->induk[$i]][$j][$k];
                        $individu_baru[$i + 1][$j][$k] = $this->individu[$this->induk[$i + 1]][$j][$k];
                    }
                }
            }
            else {
                // Ketika nilai random lebih dari nilai probabilitas pertukaran, maka jadwal baru sama dengan jadwal terpilih.
                for ($j = 0; $j < $this->jumlah_posisi; $j++) {
                    for ($k = 0; $k < 4; $k++) {
                        $individu_baru[$i][$j][$k] = $this->individu[$this->induk[$i]][$j][$k];
                        $individu_baru[$i + 1][$j][$k] = $this->individu[$this->induk[$i + 1]][$j][$k];
                    }
                }
            }
            for ($j = 0; $j < $this->jumlah_posisi; $j++) {
                for ($k = 0; $k < 4; $k++) {
                    $this->individu[$i][$j][$k] = $individu_baru[$i][$j][$k];
                    $this->individu[$i + 1][$j][$k] = $individu_baru[$i + 1][$j][$k];
                }
            }
        }
        for ($i = 0; $i < $this->populasi; $i += 2) {
            for ($j = 0; $j < $this->jumlah_posisi; $j++) {
                for ($k = 0; $k < 4; $k++) {
                    $this->individu[$i][$j][$k] = $individu_baru[$i][$j][$k];
                    $this->individu[$i + 1][$j][$k] = $individu_baru[$i + 1][$j][$k];
                }
            }
        }
    }

    function mutasi() {
        $fitness = [];
        // Proses perandoman atau penggantian komponen untuk tiap jadwal baru.
        $random = mt_rand(0, mt_getrandmax() - 1) / mt_getrandmax();
        for ($i = 0; $i < $this->populasi; $i++) {
            // Jika nilai random kurang dari nilai probabilitas mutasi maka terjadi penggantian karyawan.
            if ($random < $this->mutasi) {
                $kromosom = mt_rand(0, $this->jumlah_posisi - 1);
                $this->individu[$i][$kromosom][3] = $this->karyawan[mt_rand(0, count($this->karyawan) - 1)];
            }
            $fitness[$i] = $this->cek_fitness($i);
        }
        return $fitness;
    }

    function ambil_individu($individu) {
        $individu_solusi = [[]];
        for ($i = 0; $i < $this->jumlah_posisi; $i++) {
            $individu_solusi[$i][0] = $i;
            $individu_solusi[$i][1] = intval($this->individu[$individu][$i][1]);
            $individu_solusi[$i][2] = intval($this->individu[$individu][$i][2]);
            $individu_solusi[$i][3] = intval($this->individu[$individu][$i][3]);
        }
        return $individu_solusi;
    }

    ////////

    function render_page_view($data) {
        $this->load->view('page', $data);
    }

    function index() {
        $data = [];
        $data['page_name'] = 'beranda';
        $data['page_title'] = 'e-Penjadwalan Karyawan';
        $this->render_page_view($data);
    }

    function karyawan() {
        $data = [];
        $data['page_title'] = 'Karyawan';
        $url = base_url('web/karyawan');
        $per_page = 20;
        // Membuat paginasi AJAX agar muncul 20 karyawan saja per halaman.
        $this->pagination->initialize(admin_paginate($url, $this->db->from(
                                "karyawan WHERE terhapus = 'N'"
                        )->count_all_results(), $per_page, 3));
        $this->model_karyawan->limit = $per_page;
        if ($this->uri->segment(3)) {
            $this->model_karyawan->offset = $this->uri->segment(3);
        }
        else {
            $this->model_karyawan->offset = 0;
        }
        $data['start_number'] = $this->model_karyawan->offset;
        $this->model_karyawan->sort = 'RIGHT(nip, 12)';
        $this->model_karyawan->order = 'ASC';
        $data['rs_karyawan'] = $this->model_karyawan->get();
        // AJAX untuk melihat daftar 20 karyawan selanjutnya / sebelumnya tanpa muat ulang.
        if ($this->input->post('ajax')) {
            $this->load->view('karyawan_ajax', $data);
        }
        else {
            $data['page_name'] = 'karyawan';
            $this->render_page_view($data);
        }
    }

    function tambah_karyawan() {
        $data = [];
        if (!empty($_POST)) {
            $this->form_validation->set_rules('nip', 'NIP', 'required');
            $this->form_validation->set_rules('nama', 'Nama', 'required|is_unique[karyawan.nama]');
            $this->form_validation->set_rules('kode_jabatan', 'Jabatan', 'required');
            $this->form_validation->set_rules('golongan', 'Golongan', 'required');
            if ($this->form_validation->run()) {
                $data['nip'] = $this->input->post('nip');
                $data['nama'] = $this->input->post('nama');
                $data['kode_jabatan'] = $this->input->post('kode_jabatan');
                $data['golongan'] = $this->input->post('golongan');
                if (!IS_TEST) {
                    $this->model_karyawan->insert($data);
                    $data['msg'] = 'Data berhasil ditambahkan.';
                    $data['clear_text_box'] = TRUE;
                }
                else {
                    $data['msg'] = '[Read-Only]';
                }
            }
            else {
                $data['msg'] = validation_errors();
            }
        }
        $data['page_name'] = 'tambah_karyawan';
        $data['page_title'] = 'Tambah Karyawan';
        $data['rs_jabatan'] = $this->model_jabatan->get();
        $this->render_page_view($data);
    }

    function ubah_karyawan($kode) {
        $data = [];
        if (!empty($_POST)) {
            $this->form_validation->set_rules('nip', 'NIK', 'required');
            $this->form_validation->set_rules('nama', 'Nama', 'required');
            $this->form_validation->set_rules('kode_jabatan', 'Jabatan', 'required');
            $this->form_validation->set_rules('golongan', 'Golongan', 'required');
            if ($this->form_validation->run()) {
                $data['nip'] = $this->input->post('nip');
                $data['nama'] = $this->input->post('nama');
                $data['kode_jabatan'] = $this->input->post('kode_jabatan');
                $data['golongan'] = $this->input->post('golongan');
                if (!IS_TEST) {
                    $this->model_karyawan->update($kode, $data);
                    $data['msg'] = 'Data berhasil diubah.';
                }
                else {
                    $data['msg'] = '[Read-Only]';
                }
            }
            else {
                $data['msg'] = validation_errors();
            }
        }
        $data['page_name'] = 'ubah_karyawan';
        $data['page_title'] = 'Ubah Karyawan';
        $data['rs_karyawan'] = $this->model_karyawan->get_by_kode($kode);
        $data['rs_jabatan'] = $this->model_jabatan->get();
        $this->render_page_view($data);
    }

    function hapus_karyawan($kode) {
        if (!IS_TEST) {
            $this->model_karyawan->delete($kode);
            $this->model_waktu_tidak_bersedia->delete($kode);
            $this->session->set_flashdata('msg', 'Data berhasil dihapus.');
        }
        else {
            $this->session->set_flashdata('msg', '[Read-Only]');
        }
        redirect(base_url('web/karyawan'), 'reload');
    }

    function cari_karyawan() {
        $data = [];
        $search_query = $this->input->post('search_query');
        $data['rs_karyawan'] = $this->model_karyawan->get_by_nama($search_query);
        $data['page_name'] = 'cari_karyawan';
        $data['page_title'] = 'Cari Karyawan';
        $data['search_query'] = $search_query;
        $data['start_number'] = 0;
        $this->render_page_view($data);
    }

    function jabatan() {
        $data = [];
        $data['page_name'] = 'jabatan';
        $data['page_title'] = 'Jabatan';
        $data['rs_jabatan'] = $this->model_jabatan->get();
        $this->render_page_view($data);
    }

    function tambah_jabatan() {
        $data = [];
        if (!empty($_POST)) {
            $this->form_validation->set_rules('nama', 'Jabatan', 'required');
            if ($this->form_validation->run()) {
                $data['nama'] = $this->input->post('nama');
                if (!IS_TEST) {
                    $this->model_jabatan->insert($data);
                    $data['msg'] = 'Data berhasil ditambahkan.';
                    $data['clear_text_box'] = TRUE;
                }
                else {
                    $data['msg'] = '[Read-Only]';
                }
            }
            else {
                $data['msg'] = validation_errors();
            }
        }
        $data['page_name'] = 'tambah_jabatan';
        $data['page_title'] = 'Tambah Jabatan';
        $this->render_page_view($data);
    }

    function ubah_jabatan($kode) {
        $data = [];
        if (!empty($_POST)) {
            $this->form_validation->set_rules('nama', 'Nama Jabatan', 'required');
            if ($this->form_validation->run()) {
                $data['nama'] = $this->input->post('nama');
                if (!IS_TEST) {
                    $this->model_jabatan->update($kode, $data);
                    $data['msg'] = 'Data berhasil diubah.';
                }
                else {
                    $data['msg'] = '[Read-Only]';
                }
            }
            else {
                $data['msg'] = validation_errors();
            }
        }
        $data['page_name'] = 'ubah_jabatan';
        $data['page_title'] = 'Ubah Jabatan';
        $data['rs_jabatan'] = $this->model_jabatan->get_by_kode($kode);
        $this->render_page_view($data);
    }

    function hapus_jabatan($kode) {
        if (!IS_TEST) {
            $this->model_jabatan->delete($kode);
            $this->session->set_flashdata('msg', 'Data berhasil dihapus.');
        }
        else {
            $this->session->set_flashdata('msg', '[Read-Only]');
        }
        redirect(base_url('web/jabatan'), 'reload');
    }

    function sif() {
        $data = [];
        $data['page_name'] = 'sif';
        $data['page_title'] = 'Sif';
        $data['rs_sif'] = $this->model_sif->get();
        $this->render_page_view($data);
    }

    function tambah_sif() {
        $data = [];
        if (!empty($_POST)) {
            $this->form_validation->set_rules('nama', 'Nama Sif', 'required');
            $this->form_validation->set_rules('karyawan_per_sif', 'Nama Sif', 'required');
            $this->form_validation->set_rules('kode_jabatan', 'Nama Sif', 'required');
            if ($this->form_validation->run()) {
                $data['nama'] = $this->input->post('nama');
                $data['karyawan_per_sif'] = $this->input->post('karyawan_per_sif');
                $data['kode_jabatan'] = $this->input->post('kode_jabatan');
                $data['kode_pengubah'] = $_SESSION['user_id'];
                if (!IS_TEST) {
                    $this->model_sif->insert($data);
                    $data['msg'] = 'Data berhasil ditambahkan.';
                    $data['clear_text_box'] = TRUE;
                }
                else {
                    $data['msg'] = '[Read-Only]';
                }
            }
            else {
                $data['msg'] = validation_errors();
            }
        }
        $data['page_name'] = 'tambah_sif';
        $data['page_title'] = 'Tambah Sif';
        $data['rs_jabatan'] = $this->model_jabatan->get();
        $this->render_page_view($data);
    }

    function ubah_sif($kode) {
        $data = [];
        if (!empty($_POST)) {
            $this->form_validation->set_rules('nama', 'Nama Sif', 'required');
            $this->form_validation->set_rules('karyawan_per_sif', 'Karyawan per Sif', 'required');
            $this->form_validation->set_rules('kode_jabatan', 'Kode Jabatan', 'required');
            if ($this->form_validation->run()) {
                $data['nama'] = $this->input->post('nama');
                $data['karyawan_per_sif'] = $this->input->post('karyawan_per_sif');
                $data['kode_jabatan'] = $this->input->post('kode_jabatan');
                $data['kode_pengubah'] = $_SESSION['user_id'];
                if (!IS_TEST) {
                    $this->model_sif->update($kode, $data);
                    $data['msg'] = 'Data berhasil diubah.';
                }
                else {
                    $data['msg'] = '[Read-Only]';
                }
            }
            else {
                $data['msg'] = validation_errors();
            }
        }
        $data['page_name'] = 'ubah_sif';
        $data['page_title'] = 'Ubah Sif';
        $data['rs_sif'] = $this->model_sif->get_by_kode($kode);
        $data['rs_jabatan'] = $this->model_jabatan->get();
        $this->render_page_view($data);
    }

    function hapus_sif($kode) {
        if (!IS_TEST) {
            $this->model_sif->delete($kode);
            $this->session->set_flashdata('msg', 'Data berhasil dihapus.');
        }
        else {
            $this->session->set_flashdata('msg', '[Read-Only]');
        }
        redirect(base_url('web/sif'), 'reload');
    }

    function hari() {
        $data = [];
        $data['page_name'] = 'hari';
        $data['page_title'] = 'Hari';
        $data['rs_hari'] = $this->model_hari->get();
        $this->render_page_view($data);
    }

    function tambah_hari() {
        $data = [];
        if (!empty($_POST)) {
            $this->form_validation->set_rules('nama', 'Nama Hari', 'required|is_unique[hari.nama]');
            if ($this->form_validation->run()) {
                $data['nama'] = $this->input->post('nama');
                if (!IS_TEST) {
                    $this->model_hari->insert($data);
                    $data['msg'] = 'Data berhasil ditambahkan.';
                    $data['clear_text_box'] = true;
                }
                else {
                    $data['msg'] = '[Read-Only]';
                }
            }
            else {
                $data['msg'] = validation_errors();
            }
        }
        $data['page_name'] = 'tambah_hari';
        $data['page_title'] = 'Tambah Hari';
        $this->render_page_view($data);
    }

    function ubah_hari($kode) {
        $data = [];
        if (!empty($_POST)) {
            $this->form_validation->set_rules('nama', 'Nama Hari', 'required');
            if ($this->form_validation->run()) {
                $data['nama'] = $this->input->post('nama');
                if (!IS_TEST) {
                    $this->model_hari->update($kode, $data);
                    $data['msg'] = 'Data berhasil diubah.';
                }
                else {
                    $data['msg'] = '[Read-Only]';
                }
            }
            else {
                $data['msg'] = validation_errors();
            }
        }
        $data['page_name'] = 'ubah_hari';
        $data['page_title'] = 'Ubah Hari';
        $data['rs_hari'] = $this->model_hari->get_by_kode($kode);
        $this->render_page_view($data);
    }

    function hapus_hari($kode) {
        if (!IS_TEST) {
            $this->model_hari->delete($kode);
            $this->session->set_flashdata('msg', 'Data berhasil dihapus.');
        }
        else {
            $this->session->set_flashdata('msg', '[Read-Only]');
        }
        redirect(base_url() . 'web/hari', 'reload');
    }

    function waktu_tidak_bersedia($kode = NULL) {
        $data = [];
        if ($kode === NULL) {
            $kode = $this->db->query("SELECT kode FROM karyawan ORDER BY nama LIMIT 1")->row()->kode;
        }
        $kode_jabatan = $this->db->query("SELECT kode_jabatan FROM karyawan WHERE kode = $kode")->row()->kode_jabatan;
        if (array_key_exists('array_ketidaksediaan', $_POST) && !empty($_POST['array_ketidaksediaan'])) {
            if (!IS_TEST) {
                $this->db->query("DELETE FROM waktu_tidak_bersedia WHERE kode_karyawan = $kode");
                foreach ($_POST['array_ketidaksediaan'] as $tidak_bersedia) {
                    $waktu_tidak_bersedia = explode('-', $tidak_bersedia);
                    $this->db->query("INSERT INTO waktu_tidak_bersedia (kode_karyawan, kode_hari, kode_sif, kode_pengubah) VALUES ($waktu_tidak_bersedia[0], $waktu_tidak_bersedia[1], $waktu_tidak_bersedia[2], $_SESSION[user_id])");
                }
                $data['msg'] = 'Data berhasil diubah.';
            }
            else {
                $data['msg'] = '[Read-Only]';
            }
        }
        elseif (!empty($_POST['hide_me']) && empty($_POST['array_ketidaksediaan'])) {
            $this->db->query("DELETE FROM waktu_tidak_bersedia WHERE kode_karyawan = $kode");
            $data['msg'] = 'Data berhasil diubah.';
        }
        // rs = resulting data
        $data['rs_karyawan'] = $this->model_karyawan->get_all();
        $data['rs_waktu_tidak_bersedia'] = $this->model_waktu_tidak_bersedia->get_by_kode($kode);
        $data['rs_hari'] = $this->model_hari->get();
        $data['rs_sif'] = $this->model_sif->get_by_jabatan($kode_jabatan);
        $data['page_name'] = 'waktu_tidak_bersedia';
        $data['page_title'] = 'Waktu Tidak Bersedia Karyawan';
        $data['kode_karyawan'] = $kode;
        $this->render_page_view($data);
    }

    function penjadwalan() {
        $data = [];
        if (!empty($_POST)) {
            $this->form_validation->set_rules('kode_jabatan', 'Jabatan', 'required');
            $this->form_validation->set_rules('max_hari_kerja', 'Maksimum Hari Kerja', 'numeric|required');
            $this->form_validation->set_rules('jumlah_populasi', 'Jumlah Populasi', 'numeric|required');
            $this->form_validation->set_rules('jumlah_generasi', 'Jumlah Generasi', 'numeric|required');
            $this->form_validation->set_rules('probabilitas_crossover', 'Probabilitas Crossover', 'numeric|required');
            $this->form_validation->set_rules('probabilitas_mutasi', 'Probabilitas Mutasi', 'numeric|required');
            if ($this->form_validation->run()) {
                $this->jabatan = $this->input->post('kode_jabatan');
                $this->max_hari_kerja = $this->input->post('max_hari_kerja');
                $this->populasi = $this->input->post('jumlah_populasi');
                $jumlah_generasi = $this->input->post('jumlah_generasi');
                $this->crossover = $this->input->post('probabilitas_crossover');
                $this->mutasi = $this->input->post('probabilitas_mutasi');
                $data['kode_jabatan'] = $this->jabatan;
                $data['max_hari_kerja'] = $this->max_hari_kerja;
                $data['jumlah_populasi'] = $this->populasi;
                $data['jumlah_generasi'] = $jumlah_generasi;
                $data['probabilitas_crossover'] = $this->crossover;
                $data['probabilitas_mutasi'] = $this->mutasi;
                $rs_jadwal = $this->db->query("SELECT kode FROM karyawan");
                if ($rs_jadwal->num_rows() === 0) {
                    $data['msg'] = 'Tidak ada karyawan saat ini.<br>Data tampil dari proses sebelumnya.';
                }
                else {
                    $this->ambil_data();
                    $this->inisiasi();
                    $ditemukan = FALSE;
                    for ($i = 0; $i < $jumlah_generasi; $i++) {
                        $this->seleksi($this->hitung_fitness());
                        $this->crossover();
                        $fitness_mutasi = $this->mutasi();
                        for ($j = 0; $j < count($fitness_mutasi); $j++) {
                            if ($fitness_mutasi[$j] == 1) {
                                $this->model_jadwal_kerja->truncate();
                                $jadwal_kerja = $this->ambil_individu($j);
                                for ($k = 0; $k < count($jadwal_kerja); $k++) {
                                    $kode_hari = intval($jadwal_kerja[$k][1]);
                                    $kode_sif = intval($jadwal_kerja[$k][2]);
                                    $kode_karyawan = intval($jadwal_kerja[$k][3]);
                                    $this->db->query(
                                            "INSERT INTO jadwal_kerja (kode_karyawan, kode_sif, kode_hari) VALUES ($kode_karyawan, $kode_sif, $kode_hari)"
                                    );
                                }
                                $ditemukan = TRUE;
                                break;
                            }
                        }
                        if ($ditemukan) {
                            $this->model_jadwal_kerja->update_pembuat($_SESSION['user_id']);
                            break;
                        }
                    }
                    if (!$ditemukan) {
                        $data['msg'] = 'Tidak ditemukan solusi optimal.<br>Tips:<br>'
                                . '- Cek jumlah karyawan, jam kerja, dan sif yang ditentukan.<br>'
                                . '- Lakukan perhitungan ulang dilain waktu atau coba lagi.<br>'
                                . '- Naikkan jumlah populasi atau generasi (perhitungan lebih lama).<br>';
                    }
                }
            }
            else {
                $data['msg'] = validation_errors();
            }
        }
        $data['page_name'] = 'penjadwalan';
        $data['page_title'] = 'Penjadwalan';
        $data['rs_jabatan'] = $this->model_jabatan->get();
        $data['rs_jadwal'] = $this->model_jadwal_kerja->get();
        $this->render_page_view($data);
    }

    function excel_report() {
        $query = $this->model_jadwal_kerja->get();
        $this->load->library('PHPExcel');
        $this->load->library('PHPExcel/IOFactory');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("Ekspor")->setDescription(
                "Hasil penjadwalan karyawan dengan algoritma genetika."
        );
        $objPHPExcel->setActiveSheetIndex(0);
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }
        $row = 2;
        foreach ($query->result() as $data) {
            $col = 0;
            foreach ($fields as $field) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
            $row++;
        }
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        //header('content-type: application/vnd.ms-excel');
        header('content-disposition: attachment; filename="Jadwal_' . date('dMy') . '.xls"');
        //header('cache-control: max-age=0');
        $objWriter->save('php://output');
    }

}
