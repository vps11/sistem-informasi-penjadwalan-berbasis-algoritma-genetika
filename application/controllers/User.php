<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('model_pengguna');
    }

    function render_page_view($data) {
        $this->load->view('page', $data);
    }

    function index() {
        $data = [];
        $data['page_name'] = 'login';
        $data['page_title'] = 'e-Penjadwalan Karyawan';
        $this->render_page_view($data);
    }

    public function register() {
        $data = [];
        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[4]|is_unique[pengguna.username]', array('is_unique' => 'This username already exists. Please choose another one.'));
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('password_confirm', 'Konfirmasi Password', 'trim|required|min_length[6]|matches[password]');
        if ($this->form_validation->run()) {
            $username = $this->input->post('username');
            $email = '';
            $password = $this->input->post('password');
            if ($this->model_pengguna->create_user($username, $email, $password)) {
                $data['msg'] = 'Berhasil, berhasil, berhasil, hore!';
                $data['page_name'] = 'login';
                $data['page_title'] = 'e-Penjadwalan Karyawan';
                $this->render_page_view($data);
            }
            else {
                $data['msg'] = 'There was a problem creating your new account. Please try again.';
            }
        }
        else {
            $data['msg'] = validation_errors();
        }
        $data['msg'] = 'Selamat datang di Sistem Penjadwalan Karyawan. Silahkan daftar.';
        $data['page_name'] = 'register';
        $data['page_title'] = 'Registrasi';
        $this->render_page_view($data);
    }

    public function login() {
        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
            redirect('web');
        }
        else {
            $_SESSION['logged_in'] = FALSE;
            $data = [];
            if (!empty($_POST)) {
                $this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
                $this->form_validation->set_rules('password', 'Password', 'required');
                if ($this->form_validation->run()) {
                    $data['username'] = $this->input->post('username');
                    $data['password'] = $this->input->post('password');
                    if ($this->model_pengguna->resolve_user_login($data['username'], $data['password'])) {
                        $user_id = $this->model_pengguna->get_user_id_from_username($data['username']);
                        $user = $this->model_pengguna->get_user($user_id);
                        $_SESSION['user_id'] = $user->id;
                        $_SESSION['username'] = $user->username;
                        $_SESSION['logged_in'] = TRUE;
                        redirect('web');
                    }
                    else {
                        $data['msg'] = 'Wrong username or password.';
                    }
                }
                else {
                    $data['msg'] = validation_errors();
                }
            }
            $data['page_name'] = 'login';
            $data['page_title'] = 'Login';
            $this->render_page_view($data);
        }
    }

    public function logout() {
        $data = [];
        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }
            redirect('user/login');
        }
        else {
            redirect('/');
        }
    }

}
