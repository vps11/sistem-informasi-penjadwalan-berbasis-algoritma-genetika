<?php
foreach ($rs_jabatan->result() as $jabatan) {
    
}
?>
<div class="content">
    <div class="header">
        <h1 class="page-title"><?php echo $page_title; ?></h1>
    </div>
    <ul class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Beranda</a> <span class="divider">/</span></li>
        <li><a href="<?php echo base_url(); ?>web/jabatan">Jabatan</a> <span class="divider">/</span></li>
        <li class="active"><?php echo $page_title; ?></li>
    </ul>
    <div class="container-fluid">
        <div class="row-fluid">
            <?php if (isset($msg)) { ?>                        
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">�</button>                
                    <?php echo $msg; ?>
                </div>
            <?php } ?>
            <form id="tab" method="POST">
                <label>Nama Jabatan</label>
                <input id="jabatan" type="text" value="<?php echo $jabatan->nama; ?>" name="jabatan" class="input-xlarge" />
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="<?php echo base_url() . 'web/jabatan'; ?>"><button type="button" class="btn">Cancel</button></a>
                </div>
            </form>
        </div>
    </div>
</div>      