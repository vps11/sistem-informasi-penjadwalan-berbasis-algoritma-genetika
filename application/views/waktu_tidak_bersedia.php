<div class="content">
    <div class="header">
        <h1 class="page-title"><?php echo $page_title; ?></h1>
    </div>
    <ul class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Beranda</a> <span class="divider">/</span></li>
        <li class="active"><?php echo $page_title; ?></li>
    </ul>
    <div class="container-fluid">
        <?php if (isset($msg)) { ?>
            <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <?php echo $msg; ?>
            </div>
        <?php } ?>
        <div class="row-fluid">
            <form class="form-inline" method="POST">
                <div class="form">
                    <select id = "kode_karyawan" name="kode_karyawan" class="input-xlarge" onchange="change_karyawan_tidak_bersedia()">
                        <?php foreach ($rs_karyawan->result() as $karyawan) { ?>
                            <option value="<?php echo $karyawan->kode; ?>" <?php echo isset($kode_karyawan) ? ($kode_karyawan === $karyawan->kode ? 'selected' : '') : ''; ?>><?php echo $karyawan->nama; ?></option>
                        <?php } ?>
                    </select>
                    <input type="hidden" name="hide_me" value="hide_me">
                    <button type="submit" class="btn">Simpan Perubahan</button>
                </div>
                <br>
                <div class="widget-content">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Hari</th>
                                <th>Sif</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($rs_hari->result() as $hari) {
                                foreach ($rs_sif->result() as $sif) {
                                    ?>
                                    <tr>
                                        <td><?php echo $hari->nama; ?></td>
                                        <td><?php echo $sif->nama; ?></td>
                                        <?php
                                        $status = '';
                                        foreach ($rs_waktu_tidak_bersedia->result() as $waktu_tidak_bersedia) {
                                            if ($waktu_tidak_bersedia->kode_hari === $hari->kode && $waktu_tidak_bersedia->kode_sif === $sif->kode) {
                                                $status = 'checked';
                                            }
                                        }
                                        ?>
                                        <td>
                                            <label><input type="checkbox" name="array_ketidaksediaan[]" value="<?php echo $kode_karyawan . '-' . $hari->kode . '-' . $sif->kode ?>" <?php echo $status; ?>> Tidak Bersedia</label>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>