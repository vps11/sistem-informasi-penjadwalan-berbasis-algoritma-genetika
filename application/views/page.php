<!DOCTYPE html>
<html lang="id">
    <head>
        <meta charset="utf-8" />
        <title><?= $page_title; ?></title>
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="icon" href="<?= base_url('favicon.ico'); ?>" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/lib/bootstrap/css/bootstrap.css'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/theme.css'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/badger.min.css'); ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/lib/font-awesome/css/font-awesome.css'); ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/lib/datepicker/css/datepicker.css'); ?>" />
        <script src="<?= base_url('assets/lib/jquery-latest.min.js'); ?>" type="text/javascript"></script>	  
        <style type="text/css">
            body .frmModalMsg {
                width: 740px;
                margin-left: -280px;
            }

            #line-chart {
                height:300px;
                width:800px;
                margin: 0px auto;
                margin-top: 1em;
            }

            .brand { font-family: georgia, serif; }

            .brand .first {
                color: #ccc;
                font-style: italic;
            }

            .brand .second {
                color: #fff;
                font-weight: bold;
            }

            #loading-div-background{
                display: none;
                opacity: 0.85;
                position: fixed;
                top: 0;
                left: 0;
                background: #fff;
                width: 100%;
                height: 100%;
            }

            #loading-div{
                width: 300px;
                height: 150px;
                text-align: center;
                position: absolute;
                left: 50%;
                top: 50%;
                margin-left: -150px;
                margin-top: -100px;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#loading-div-background");
<?php if (isset($clear_text_box)) { ?>
                    $('input[type=text]').each(function () {
                        $(this).val('');
                    });
<?php } ?>
            });

            function show_progress_gif() {
                $("#loading-div-background").show();
            }

            function change_karyawan_tidak_bersedia() {
                var kode_karyawan = document.getElementById('kode_karyawan');
                window.location.href = "<?= base_url('web/waktu_tidak_bersedia/'); ?>" + kode_karyawan.options[kode_karyawan.selectedIndex].value;
            }

            function delete_row(link, kode) {
                var answer = confirm('Anda yakin ingin menghapus data ini?');
                if (answer) {
                    $.ajax({
                        type: "POST",
                        async: false,
                        cache: false,
                        url: "<?= base_url(); ?>" + link + kode,
                        success: function (msg) {
                            var tr = $('#row_' + kode);
                            tr.css("background-color", "#FF3700");
                            tr.fadeOut(400, function () {
                                tr.remove();
                            });
                        }
                    });

                }
                return false;
            }

            $(function () {
                applyPagination();
                function applyPagination() {
                    $("#ajax_paging a").click(function () {
                        var url = $(this).attr("href");
                        $.ajax({
                            type: "POST",
                            data: "ajax=1",
                            url: url,
                            success: function (msg) {
                                $('#content_ajax').fadeOut(0, function () {
                                    $('#content_ajax').html(msg);
                                    $("#content_ajax").removeAttr("style");
                                    applyPagination();
                                }).fadeIn(0);
                            }
                        });
                        return false;
                    });
                }
            });
        </script>
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!-- Le fav and touch icons -->
        <link rel="shortcut icon" href="<?= base_url('assets/ico/favicon.ico'); ?>" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= base_url('assets/ico/apple-touch-icon-144-precomposed.png'); ?>" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= base_url('assets/ico/apple-touch-icon-114-precomposed.png'); ?>" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= base_url('assets/ico/apple-touch-icon-72-precomposed.png'); ?>" />
        <link rel="apple-touch-icon-precomposed" href="<?= base_url('assets/ico/apple-touch-icon-57-precomposed.png'); ?>" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <!--[if lt IE 7 ]> 
    <body class="ie ie6">
       <![endif]-->
    <!--[if IE 7 ]> 
    <body class="ie ie7 ">
       <![endif]-->
    <!--[if IE 8 ]> 
    <body class="ie ie8 ">
       <![endif]-->
    <!--[if IE 9 ]> 
    <body class="ie ie9 ">
       <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!--> 
    <body class="">
        <!--<![endif]-->
        <div class="navbar">
            <div class="navbar-inner">
                <a class="brand"><span class="first">e-Penjadwalan Karyawan</span> <span class="second"></span></a>
                <ul class="nav navbar-nav navbar-right">
                    <?php if (isset($_SESSION['username']) && $_SESSION['logged_in']) { ?>
                        <li><a><i class="icon-user"></i><?= $_SESSION['username']; ?></a></li>
                        <li><a href="<?= base_url('user/logout'); ?>"><i class="icon-signout"></i>Keluar</a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="sidebar-nav">
            <a href="<?= base_url(); ?>" class="nav-header"><i class="icon-home"></i>Beranda</a>
            <a class="nav-header" ><i class="icon-book"></i>Data</a>
            <ul id="content-menu" class="nav nav-list collapse in">
                <li><a href="<?= base_url('web/karyawan'); ?>"><i class="icon-user"></i>Karyawan</a></li>
                <li><a href="<?= base_url('web/jabatan'); ?>"><i class="icon-star"></i>Jabatan</a></li>
                <li><a href="<?= base_url('web/sif'); ?>"><i class="icon-time"></i>Sif</a></li>
                <li><a href="<?= base_url('web/hari'); ?>"><i class="icon-calendar"></i>Hari</a></li>
                <li><a href="<?= base_url('web/waktu_tidak_bersedia'); ?>"><i class="icon-ban-circle"></i>Waktu Tidak Bersedia</a></li>
            </ul>
            <a class="nav-header" ><i class="icon-book"></i>Proses</a>
            <ul id="content-menu" class="nav nav-list collapse in">
                <li><a href="<?= base_url('web/penjadwalan'); ?>"><i class="icon-list"></i>Penjadwalan</a></li>
            </ul>
        </div>
        <?php include $page_name . ".php"; ?>
    </body>
</html>