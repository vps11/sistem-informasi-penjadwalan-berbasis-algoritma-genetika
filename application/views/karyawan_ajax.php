<div class="pagination" id="ajax_paging">
    <ul>
        <?php echo $this->pagination->create_links(); ?>
    </ul>
</div>
<div class="widget-content">            
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama</th>
                <th>NIP</th>
                <th>Jabatan</th>
                <th>Golongan</th>
                <th style="width: 65px;"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = intval($start_number) + 1;
            foreach ($rs_karyawan->result() as $karyawan) {
                ?>
                <tr>
                    <td><?php echo str_pad((int) $i, 2, 0, STR_PAD_LEFT); ?></td>
                    <td><?php echo $karyawan->nama; ?></td>
                    <td><?php echo $karyawan->nip; ?></td>
                    <td><?php echo $karyawan->jabatan; ?></td>
                    <td><?php echo $karyawan->golongan; ?></td>
                    <td>
                        <a href="<?php echo base_url() . 'web/ubah_karyawan/' . $karyawan->kode; ?>" class="btn btn-small"><i class="icon-pencil"></i></a>
                        <a href="<?php echo base_url() . 'web/hapus_karyawan/' . $karyawan->kode; ?>" class="btn btn-small" onClick="return confirm('Anda yakin ingin menghapus data ini?')" ><i class="icon-trash"></i></a>
                    </td>
                </tr>
                <?php
                $i++;
            }
            ?>
        </tbody>
    </table>
