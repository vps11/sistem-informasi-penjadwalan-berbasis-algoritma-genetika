<style>
    .block {
        background: none repeat scroll 0 0 #FFFFFF;
        border: 0px solid #CCCCCC;
    }
    help {
        color: #0088cc;
        cursor: pointer;
    }
</style>
<div class="content">
    <div class="header">
        <h1 class="page-title"><?= $page_title; ?></h1>
    </div>
    <ul class="breadcrumb">
        <li><a href="<?= base_url(); ?>">Beranda</a> <span class="divider">/</span></li>
        <li class="active"><?= $page_title; ?></li>
    </ul>
    <div class="container-fluid">
        <?php if (isset($msg)) { ?>                        
            <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">x</button>                
                <?= $msg; ?>
            </div>  
        <?php } ?>
        <div class="row-fluid">
            <form class="form" method="POST">
                <div class="block span6">
                    <label>Jabatan</label>
                    <select id="kode_jabatan" name="kode_jabatan" class="input-xlarge">
                        <?php foreach ($rs_jabatan->result() as $jabatan) { ?>
                            <option value="<?= $jabatan->kode; ?>" <?= isset($kode_jabatan) ? ($kode_jabatan === $jabatan->kode ? 'selected' : '') : ''; ?> /> <?= $jabatan->nama; ?>
                        <?php } ?>
                    </select>
                    <label>Maksimum Hari Kerja</label>
                    <input id="max_hari_kerja" type="text" value="<?= isset($max_hari_kerja) ? $max_hari_kerja : '5'; ?>" name="max_hari_kerja" class="input-xlarge"/>  
                </div>
                <div class="block span6">
                    <label>Jumlah Populasi [<help onClick="return confirm(''
                                    )">?</help>]</label>
                    <input id="jumlah_populasi" type="text" value="<?= isset($jumlah_populasi) ? $jumlah_populasi : '100'; ?>" name="jumlah_populasi" class="input-xlarge"/>  
                    <label>Jumlah Generasi [<help onClick="return confirm(''
                                    )">?</help>]</label>
                    <input id="jumlah_generasi" type="text" value="<?= isset($jumlah_generasi) ? $jumlah_generasi : '10000'; ?>" name="jumlah_generasi" class="input-xlarge"/>
                    <label>Probabilitas <i>Crossover</i> [<help onClick="return confirm(''
                                    )">?</help>]</label>
                    <input id="probabilitas_crossover" type="text" value="<?= isset($probabilitas_crossover) ? $probabilitas_crossover : '0.8'; ?>" name="probabilitas_crossover" class="input-xlarge"/>
                    <label>Probabilitas Mutasi [<help onClick="return confirm(''
                                    )">?</help>]</label>
                    <input id="probabilitas_mutasi" type="text" value="<?= isset($probabilitas_mutasi) ? $probabilitas_mutasi : '0.2'; ?>" name="probabilitas_mutasi" class="input-xlarge"/>
                </div>
                <div class="form">
                    <button type="submit" class="btn" onclick="show_progress_gif();">Proses</button>
                    <?php if ($rs_jadwal->num_rows() !== 0) { ?>
                        <button class="btn" onclick="window.open('<?= base_url('web/excel_report'); ?>'); return false;"><i class="icon-save"></i> Ekspor ke Excel</button>
                    <?php } ?>
                </div>
            </form>
            <div id="loading-div-background">
                <div id="loading-div" class="ui-corner-all">
                    <img src="<?= base_url(); ?>assets/img/please_wait.gif" alt="Processing..."/>
                    <br><i>Server</i> sedang memproses, silahkan tunggu.<br>Jangan tutup halaman ini.
                </div>
            </div>
            <?php if ($rs_jadwal->num_rows() === 0): ?>
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">�</button>             
                    Tidak ada data.
                </div>
            <?php else: ?>
                <div id="content_ajax">
                    <div class="widget-content">            
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Hari</th>
                                    <th>Sif</th>
                                    <th>Karyawan</th>
                                    <th>Jabatan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($rs_jadwal->result() as $jadwal) {
                                    ?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td><?= $jadwal->hari; ?></td>
                                        <td><?= $jadwal->sif; ?></td>
                                        <td><?= $jadwal->karyawan; ?></td>
                                        <td><?= $jadwal->jabatan; ?></td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>      
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>