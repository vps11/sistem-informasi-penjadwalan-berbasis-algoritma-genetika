<div class="content" style="margin: 0;">
    <div class="header">
        <h1 class="page-title"><?php echo $page_title; ?></h1>
    </div>
    <div class="container-fluid" style="padding-top: 20px; text-align: center;">
        <div class="row-fluid">
            <?php if (isset($msg)) { ?>                        
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">�</button>                
                    <?php echo $msg; ?>
                </div>  
            <?php } ?>
            <form id="tab" method="POST" >
                <label for="username">Username</label>
                <input id="username" type="text" value="" name="username" class="input-xlarge" />
                <label for="password">Password</label>
                <input id="password" type="password" value="" name="password" class="input-xlarge" />
                <div class="form">
                    <button type="submit" class="btn">Login</button>
                    <a href="<?php echo base_url('user/register'); ?>"><button type="button" class="btn"><i class="icon-plus"></i> Registrasi</button></a>
                </div>
            </form>
        </div>
    </div>
</div>