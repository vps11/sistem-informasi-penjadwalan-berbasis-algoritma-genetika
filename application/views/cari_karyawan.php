<div class="content">
    <div class="header">
        <h1 class="page-title"><?php echo $page_title; ?></h1>
    </div>
    <ul class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Beranda</a> <span class="divider">/</span></li>
        <li class="active"><?php echo $page_title; ?></li>
    </ul>
    <div class="container-fluid">
        <div class="row-fluid">
            <a href="<?php echo base_url() . 'web/tambah_karyawan'; ?>"> <button class="btn btn-primary pull-right"><i class="icon-plus"></i> Karyawan Baru</button></a>
            <form class="form-inline" method="POST" action="<?php echo base_url() . 'web/cari_karyawan'; ?>">
                <input type="text" placeholder="Nama" name="search_query" value="<?php echo isset($search_query) ? $search_query : ''; ?>">
                <button type="submit" class="btn"><i class="icon-search"></i></button>
                <a href="<?php echo base_url() . 'web/karyawan'; ?>"><button type="button" class="btn"><i class="icon-remove"></i></button></a>
            </form>
            <?php if ($rs_karyawan->num_rows() === 0): ?>
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">�</button>             
                    Tidak ada data.
                </div>  
            <?php else: ?> 
                <div class="widget-content">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>NIP</th>
                                <th>Jabatan</th>
                                <th>Golongan</th>
                                <th style="width: 65px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = intval($start_number) + 1;
                            foreach ($rs_karyawan->result() as $karyawan) {
                                ?>
                                <tr>
                                    <td><?php echo str_pad((int) $i, 2, 0, STR_PAD_LEFT); ?></td>
                                    <td><?php echo $karyawan->nama; ?></td>
                                    <td><?php echo $karyawan->nip; ?></td>
                                    <td><?php echo $karyawan->jabatan; ?></td>
                                    <td><?php echo $karyawan->golongan; ?></td>                   
                                    <td>
                                        <a href="<?php echo base_url() . 'web/ubah_karyawan/' . $karyawan->kode; ?>" class="btn btn-small"><i class="icon-pencil"></i></a>
                                        <a href="<?php echo base_url() . 'web/hapus_karyawan/' . $karyawan->kode; ?>" class="btn btn-small" onClick="return confirm('Anda yakin ingin menghapus data ini?')" ><i class="icon-trash"></i></a>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>