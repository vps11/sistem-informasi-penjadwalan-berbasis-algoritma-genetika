<div class="content" style="margin: 0;">
    <div class="header">
        <h1 class="page-title"><?php echo $page_title; ?></h1>
    </div>
    <div class="container-fluid" style="padding-top: 20px; text-align: center;">
        <div class="row-fluid">
            <?php if (isset($msg)) { ?>                        
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">�</button>                
                    <?php echo $msg; ?>
                </div>  
            <?php } ?>
            <form id="tab" method="POST" >
                <label for="username">Username</label>
                <input type="text" class="form-control" id="username" name="username">
                <p class="help-block">Minimal 4 karakter, hanya huruf dan angka.</p>
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password">
                <p class="help-block">Minimal 6 karakter</p>
                <label for="password_confirm">Konfirmasi password</label>
                <input type="password" class="form-control" id="password_confirm" name="password_confirm">
                <div class="form">
                    <button type="submit" class="btn">Daftar</button>
                </div>
            </form>
        </div>
    </div>
</div>