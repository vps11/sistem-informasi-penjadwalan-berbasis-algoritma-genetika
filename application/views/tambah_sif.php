<div class="content">
    <div class="header">
        <h1 class="page-title"><?php echo $page_title; ?></h1>
    </div>
    <ul class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Beranda</a> <span class="divider">/</span></li>
        <li><a href="<?php echo base_url(); ?>web/sif">Sif</a> <span class="divider">/</span></li>
        <li class="active"><?php echo $page_title; ?></li>
    </ul>
    <div class="container-fluid">
        <div class="row-fluid">
            <?php if (isset($msg)) { ?>                        
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">�</button>                
                    <?php echo $msg; ?>
                </div>  
            <?php } ?>
            <form id="tab" method="POST" >
                <label>Nama Sif</label>
                <input id="nama" type="text" value="" name="nama" class="input-xlarge" />
                <label>Karyawan per Sif</label>
                <input id="karyawan_per_sif" type="text" value="" name="karyawan_per_sif" class="input-xlarge" />
                <label>Jabatan Sif</label>
                <select id="kode_jabatan" name="kode_jabatan" class="input-xlarge">
                    <?php foreach ($rs_jabatan->result() as $jabatan) { ?>
                        <option value="<?php echo $jabatan->kode; ?>"><?php echo $jabatan->nama; ?></option>
                    <?php } ?>
                </select>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="<?php echo base_url() . 'web/sif'; ?>"><button type="button" class="btn">Cancel</button></a>
                </div>
            </form>
        </div>
    </div>
</div>