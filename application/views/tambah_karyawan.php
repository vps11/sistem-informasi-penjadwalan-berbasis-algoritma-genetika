<div class="content">
    <div class="header">
        <h1 class="page-title"><?php echo $page_title; ?></h1>
    </div>
    <ul class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Beranda</a> <span class="divider">/</span></li>
        <li><a href="<?php echo base_url(); ?>web/karyawan">Karyawan</a> <span class="divider">/</span></li>
        <li class="active"><?php echo $page_title; ?></li>
    </ul>
    <div class="container-fluid">
        <div class="row-fluid">
            <?php if (isset($msg)) { ?>
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">�</button>                
                    <?php echo $msg; ?>
                </div>
            <?php } ?>
            <form id="tab" method="POST" >
                <label>Nama</label>
                <input id="nama" type="text" value="" name="nama" class="input-xlarge" />
                <label>NIP</label>
                <input id="nip" type="text" value="19" name="nip" class="input-xlarge" />
                <label>Jabatan</label>
                <select id="kode_jabatan" name="kode_jabatan" class="input-xlarge">
                    <?php foreach ($rs_jabatan->result() as $jabatan) { ?>
                        <option value="<?php echo $jabatan->kode; ?>"><?php echo $jabatan->nama; ?></option>
                    <?php } ?>
                </select>
                <label>Golongan</label>
                <input id="golongan" type="text" value="" name="golongan" class="input-xlarge" />    
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="<?php echo base_url() . 'web/karyawan'; ?>"><button type="button" class="btn">Cancel</button></a>
                </div>
            </form>
        </div>
    </div>
</div>